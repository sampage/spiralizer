//
//  UIScrollView+Centering.m
//  Spiralizer
//
//  Created by Sam Page on 17/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "UIScrollView+Centering.h"

@implementation UIScrollView (Centering)

- (void)centerContentOffset
{
    [self centerContentOffsetAnimated:NO];
}

- (void)centerContentOffsetAnimated:(BOOL)animated
{
    CGPoint centeredOffset = CGPointMake((self.contentSize.width / 2.0) - (self.bounds.size.width / 2.0),
                                         (self.contentSize.height / 2.0) - (self.bounds.size.height / 2.0));
    
    [self setContentOffset:centeredOffset animated:animated];
}

@end
