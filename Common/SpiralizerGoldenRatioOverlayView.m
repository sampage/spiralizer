//
//  SpiralizerGoldenRatioOverlayView.m
//  Spiralizer
//
//  Created by Sam Page on 14/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "SpiralizerGoldenRatioOverlayView.h"
#import "SpiralizerGridSegment.h"
#import "SpiralizerTraits.h"
#import "SpiralizerDefines.h"

@interface SpiralizerGoldenRatioOverlayView ()

@property (nonatomic, strong) UIView *gridView;
@property (nonatomic, strong) UIView *spiralView;

@end

static CGFloat const kLineWidthMultiplier = 0.005;

@implementation SpiralizerGoldenRatioOverlayView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.opaque = NO;
        self.userInteractionEnabled = NO;
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}

#pragma mark - Public

- (void)setTraits:(SpiralizerTraits *)traits
{
    if (_traits != traits)
    {
        _traits = traits;
    }
    
    [self setNeedsDisplay];
}

#pragma mark - Internal

/*  For our purposes we can use an approximation of a golden spial by dividing
 *  a rect and rotating at each step.
 *  see http://en.wikipedia.org/wiki/Golden_spiral#Approximations_of_the_golden_spiral
 */
- (NSArray *)calculateGridRectsForRect:(CGRect)rect lineWidth:(CGFloat)lineWidth
{
    NSMutableArray *mutableGridSegments = [NSMutableArray array];
    
    CGRect remainder = rect;
    CGRectEdge edge = CGRectMinXEdge;
    
    while (CGRectGetHeight(remainder) > lineWidth && CGRectGetWidth(remainder) > lineWidth)
    {
        CGFloat amount = (edge == CGRectMaxXEdge || edge == CGRectMinXEdge) ? CGRectGetHeight(remainder) : CGRectGetWidth(remainder);
        CGRect slice = CGRectZero;
        
        CGRectDivide(remainder, &slice, &remainder, amount, edge);
        
        SpiralizerGridSegment *gridSegment = [[SpiralizerGridSegment alloc] initWithRect:slice rectEdge:edge lineWidth:lineWidth];
        [mutableGridSegments addObject:gridSegment];
        
        edge = edge == CGRectMaxYEdge ? CGRectMinXEdge : edge + 1;
    }
    
    return [mutableGridSegments copy];
}

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    /*  While this view will be approximately sized to the golden ratio,
     *  we should assume the frame is integral and may not have the precision
     *  we need to calculate the golden spiral, so we'll create a more precisely
     *  sized rect for the calculation/drawing.
     */
    CGRect goldenRatioRect = rect;
    goldenRatioRect.size.height = goldenRatioRect.size.width / SPIRALIZER_GOLDEN_RATIO;
    
    CGFloat lineWidth = roundf(CGRectGetWidth(goldenRatioRect) * kLineWidthMultiplier);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    NSArray *gridSegments = [self calculateGridRectsForRect:goldenRatioRect lineWidth:lineWidth];
    
    SpiralizerTraits *traits = self.traits;
    
    if (traits.shouldDrawGrid)
    {
        [self drawGridSegments:gridSegments lineWidth:lineWidth context:context];
    }
    if (traits.shouldDrawSpiral)
    {
        [self drawSpiralForGridSegments:gridSegments lineWidth:lineWidth context:context];
    }
}

- (void)drawGridSegments:(NSArray *)gridSegments lineWidth:(CGFloat)lineWidth context:(CGContextRef)context
{
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(context, lineWidth);
    
    for (SpiralizerGridSegment *gridSegment in gridSegments)
    {
        CGPoint drawingStartPoint = [gridSegment drawingStartPoint];
        CGPoint drawingEndPoint = [gridSegment drawingEndPoint];
        
        CGContextMoveToPoint(context, drawingStartPoint.x, drawingStartPoint.y);
        CGContextAddLineToPoint(context, drawingEndPoint.x, drawingEndPoint.y);
    }
    
    CGContextStrokePath(context);
}

- (void)drawSpiralForGridSegments:(NSArray *)gridSegments lineWidth:(CGFloat)lineWidth context:(CGContextRef)context
{
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(context, lineWidth);
    
    UIBezierPath *spiralPath = [UIBezierPath bezierPath];
    
    for (SpiralizerGridSegment *gridSegment in gridSegments)
    {
        if (gridSegment != gridSegments.lastObject)
        {
            UIBezierPath *spiralPathSegment = [gridSegment spiralPathSegment];
            [spiralPath appendPath:spiralPathSegment];
        }
    }
    
    CGContextAddPath(context, spiralPath.CGPath);
    
    CGContextStrokePath(context);
}

@end
