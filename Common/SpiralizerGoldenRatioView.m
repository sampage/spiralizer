//
//  SpiralizerGoldenRatioView.m
//  Spiralizer
//
//  Created by Sam Page on 17/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "SpiralizerGoldenRatioView.h"
#import "SpiralizerGoldenRatioOverlayView.h"
#import "SpiralizerDefines.h"

@interface SpiralizerGoldenRatioView ()

@property (nonatomic, strong, readwrite) SpiralizerGoldenRatioOverlayView *overlayView;
@property (nonatomic, strong, readwrite) UIScrollView *scrollView;

@end

@implementation SpiralizerGoldenRatioView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.clipsToBounds = NO;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _scrollView.maximumZoomScale = 10.0;
        [self addSubview:_scrollView];
        
        _overlayView = [[SpiralizerGoldenRatioOverlayView alloc] initWithFrame:CGRectZero];
        _overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self addSubview:_overlayView];
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(size.width, size.width / SPIRALIZER_GOLDEN_RATIO);
}

- (void)setNeedsDisplay
{
    [super setNeedsDisplay];
    [self.overlayView setNeedsDisplay];
}

#pragma mark - Public

- (void)setTraits:(SpiralizerTraits *)traits
{
    if (_traits != traits)
    {
        _traits = traits;
        _overlayView.traits = traits;
    }
}

@end
