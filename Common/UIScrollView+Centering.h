//
//  UIScrollView+Centering.h
//  Spiralizer
//
//  Created by Sam Page on 17/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import UIKit;

@interface UIScrollView (Centering)

- (void)centerContentOffset;
- (void)centerContentOffsetAnimated:(BOOL)animated;

@end
