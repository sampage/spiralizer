//
//  SpiralizerGoldenRatioView.h
//  Spiralizer
//
//  Created by Sam Page on 17/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import UIKit;

@class SpiralizerGoldenRatioOverlayView;
@class SpiralizerTraits;

@interface SpiralizerGoldenRatioView : UIView

@property (nonatomic, strong, readonly) SpiralizerGoldenRatioOverlayView *overlayView;
@property (nonatomic, strong, readonly) UIScrollView *scrollView;

@property (nonatomic, weak) SpiralizerTraits *traits;

@end
