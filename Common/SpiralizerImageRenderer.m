//
//  SpiralizerImageRenderer.m
//  Spiralizer
//
//  Created by Sam Page on 15/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "SpiralizerImageRenderer.h"
#import "SpiralizerTraits.h"
#import "SpiralizerGoldenRatioOverlayView.h"
#import "SpiralizerGoldenRatioView.h"
#import "SpiralizerDefines.h"

@implementation SpiralizerImageRenderer

- (UIImage *)renderSourceImage:(UIImage *)sourceImage withTraits:(SpiralizerTraits *)traits
{
    CGSize sourceImageSize = sourceImage.size;
    CGSize scaledImageSize = CGSizeMake(sourceImageSize.width / traits.zoomScale, sourceImageSize.height / traits.zoomScale);
    
    CGRect drawingRect = CGRectMake(0.0, 0.0, scaledImageSize.width, scaledImageSize.width / SPIRALIZER_GOLDEN_RATIO);
    CGRect integralDrawingRect = CGRectIntegral(drawingRect);
    
    CGPoint centeredDrawPoint = CGPointMake((CGRectGetWidth(integralDrawingRect) - sourceImageSize.width) / 2.0,
                                            (CGRectGetHeight(integralDrawingRect) - sourceImageSize.height) / 2.0);
    
    CGPoint offsetDrawPoint = CGPointMake(centeredDrawPoint.x + (CGRectGetWidth(integralDrawingRect) * traits.relativeCenterOffset.x),
                                          centeredDrawPoint.y + (CGRectGetHeight(integralDrawingRect) * traits.relativeCenterOffset.y));
    
    UIGraphicsBeginImageContextWithOptions(integralDrawingRect.size, NO, 0.0);

    [sourceImage drawAtPoint:offsetDrawPoint];
    
#ifdef DEBUG
    SpiralizerGoldenRatioOverlayView *spiralView = [[SpiralizerGoldenRatioOverlayView alloc] initWithFrame:drawingRect];
    spiralView.traits = traits;
    [spiralView.layer renderInContext:UIGraphicsGetCurrentContext()];
#endif
    
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return renderedImage;
}

@end
