//
//  SpiralizerGoldenRatioOverlayView.h
//  Spiralizer
//
//  Created by Sam Page on 14/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import UIKit;

@class SpiralizerTraits;

@interface SpiralizerGoldenRatioOverlayView : UIView

@property (nonatomic, weak) SpiralizerTraits *traits;

@end
