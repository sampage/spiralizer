//
//  SpiralizerGridSegment.h
//  Spiralizer
//
//  Created by Sam Page on 16/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import UIKit;

@interface SpiralizerGridSegment : NSObject

@property (nonatomic, assign, readonly) CGRect rect;
@property (nonatomic, assign, readonly) CGRectEdge rectEdge;
@property (nonatomic, assign, readonly) CGFloat lineWidth;

- (instancetype)initWithRect:(CGRect)rect rectEdge:(CGRectEdge)rectEdge lineWidth:(CGFloat)lineWidth;

- (CGPoint)drawingStartPoint;
- (CGPoint)drawingEndPoint;

- (UIBezierPath *)spiralPathSegment;

@end
