//
//  SpiralizerEditingViewController.m
//  Spiralizer
//
//  Created by Sam Page on 15/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "SpiralizerEditingViewController.h"
#import "SpiralizerDefines.h"
#import "SpiralizerGoldenRatioView.h"
#import "SpiralizerGoldenRatioOverlayView.h"
#import "SpiralizerTraits.h"
#import "SpiralizerImageRenderer.h"
#import "UIScrollView+Centering.h"

@interface SpiralizerEditingViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIView *opaqueMaskView;
@property (nonatomic, strong) UIView *translucentMaskView;

@property (nonatomic, strong) UIImageView *previewImageView;

@property (nonatomic, strong) SpiralizerGoldenRatioView *goldenRatioView;
@property (nonatomic, strong) NSArray *goldenRatioViewConstraints;

@property (nonatomic, strong, readwrite) SpiralizerTraits *traits;

@end

@implementation SpiralizerEditingViewController

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInitializer];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self commonInitializer];
    }
    return self;
}

- (void)commonInitializer
{
    _traits = [[SpiralizerTraits alloc] init];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [self initializeContentView];
    [self initializeGoldenRatioView];
    [self initializePreviewImageView];
    [self initializeMaskView];
    [self initializeToolBar];
    
    [self moveScrollViewGestures];
    [self beginObservingContentViewsGeometry];
}

- (void)dealloc
{
    [self endObservingContentViewGeometry];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(__unused id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self updateGoldenRatioViewConstraintsForSize:size];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    [self updateGoldenRatioViewConstraintsForSize:self.view.bounds.size];
}

#pragma mark - View Setup

- (void)initializeContentView
{
    self.contentView = [[UIView alloc] initWithFrame:CGRectZero];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.contentView];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_contentView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_contentView]|" options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_contentView]|" options:0 metrics:nil views:viewsDictionary]];
}

- (void)initializeGoldenRatioView
{
    self.goldenRatioView = [[SpiralizerGoldenRatioView alloc] initWithFrame:CGRectZero];
    self.goldenRatioView.translatesAutoresizingMaskIntoConstraints = NO;
    self.goldenRatioView.scrollView.delegate = self;
    [self.contentView addSubview:self.goldenRatioView];
    
    self.goldenRatioView.traits = self.traits;
}

- (void)initializePreviewImageView
{
    self.previewImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.goldenRatioView.scrollView insertSubview:self.previewImageView belowSubview:self.goldenRatioView.overlayView];
}

- (void)initializeMaskView
{
    self.translucentMaskView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.translucentMaskView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.25];
    
    self.opaqueMaskView = [[UIView alloc] initWithFrame:CGRectZero];
    self.opaqueMaskView.backgroundColor = [UIColor whiteColor];
    [self.translucentMaskView addSubview:self.opaqueMaskView];
    
    self.contentView.maskView = self.translucentMaskView;
}

- (void)updateGoldenRatioViewConstraintsForSize:(CGSize)size
{
    [self.contentView removeConstraints:self.goldenRatioViewConstraints];
    
    CGFloat kGoldenRatioViewWidthMultiplier = size.width > size.height ? 0.6 : 0.9;
    
    NSMutableArray *mutableGoldenRatioViewConstraints = [NSMutableArray array];
    
    [mutableGoldenRatioViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.goldenRatioView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [mutableGoldenRatioViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.goldenRatioView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [mutableGoldenRatioViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.goldenRatioView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:kGoldenRatioViewWidthMultiplier constant:0.0]];
    [mutableGoldenRatioViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.goldenRatioView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.goldenRatioView attribute:NSLayoutAttributeWidth multiplier:1.0 / SPIRALIZER_GOLDEN_RATIO constant:0.0]];
    
    self.goldenRatioViewConstraints = [mutableGoldenRatioViewConstraints copy];
    [self.contentView addConstraints:self.goldenRatioViewConstraints];
}

- (void)initializeToolBar
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectZero];
    toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    toolBar.barStyle = UIBarStyleBlack;
    toolBar.translucent = YES;
    toolBar.tintColor = [UIColor orangeColor];
    [self.view addSubview:toolBar];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(toolBar);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[toolBar]|" options:0 metrics:nil views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[toolBar]|" options:0 metrics:nil views:viewsDictionary]];
    
    UIBarButtonItem *drawGridButton = [[UIBarButtonItem alloc] initWithTitle:@"Grid" style:UIBarButtonItemStylePlain target:self action:@selector(toggleGridDrawing)];
    UIBarButtonItem *horizontalSpacing = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *drawSpiralButton = [[UIBarButtonItem alloc] initWithTitle:@"Spiral" style:UIBarButtonItemStylePlain target:self action:@selector(toggleSpiralDrawing)];
    
    [toolBar setItems:@[drawGridButton, horizontalSpacing, drawSpiralButton]];
}

- (void)moveScrollViewGestures
{
    UIPanGestureRecognizer *panGestureRecognizer = self.goldenRatioView.scrollView.panGestureRecognizer;
    [self.view addGestureRecognizer:panGestureRecognizer];
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = self.goldenRatioView.scrollView.pinchGestureRecognizer;
    [self.view addGestureRecognizer:pinchGestureRecognizer];
}

#pragma mark - Public

- (void)setImage:(UIImage *)image
{
    if (_image != image)
    {
        _image = image;
        
        [self updateImageView];
    }
}

#pragma mark - KVO

- (void)beginObservingContentViewsGeometry
{
    [self.contentView addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew context:NULL];
    [self.contentView addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew context:NULL];
    
    [self.goldenRatioView addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew context:NULL];
    [self.goldenRatioView addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)endObservingContentViewGeometry
{
    [self.contentView removeObserver:self forKeyPath:@"bounds"];
    [self.contentView removeObserver:self forKeyPath:@"center"];
    
    [self.goldenRatioView removeObserver:self forKeyPath:@"bounds"];
    [self.goldenRatioView removeObserver:self forKeyPath:@"center"];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(__unused void *)context
{
    if (object == self.contentView)
    {
        if ([keyPath isEqualToString:@"bounds"])
        {
            self.translucentMaskView.bounds = [change[NSKeyValueChangeNewKey] CGRectValue];
        }
        else if ([keyPath isEqualToString:@"center"])
        {
            self.translucentMaskView.center = [change[NSKeyValueChangeNewKey] CGPointValue];
        }
    }
    else if (object == self.goldenRatioView)
    {
        if ([keyPath isEqualToString:@"bounds"])
        {
            self.opaqueMaskView.bounds = [change[NSKeyValueChangeNewKey] CGRectValue];
        }
        else if ([keyPath isEqualToString:@"center"])
        {
            self.opaqueMaskView.center = [change[NSKeyValueChangeNewKey] CGPointValue];
        }
        
        [self updateImageView];
    }
}

#pragma mark - Actions

- (void)toggleGridDrawing
{
    self.traits.drawGrid = !self.traits.drawGrid;
    
    [self.goldenRatioView setNeedsDisplay];
}

- (void)toggleSpiralDrawing
{
    self.traits.drawSpiral = !self.traits.drawSpiral;
    
    [self.goldenRatioView setNeedsDisplay];
}

#pragma mark - Internal

- (void)updateImageView
{
    UIImage *image = self.image;
    self.previewImageView.image = image;
    
    if (image)
    {
        UIScrollView *scrollView = self.goldenRatioView.scrollView;
        scrollView.zoomScale = 1.0;
        
        CGFloat zoomScale = MAX(CGRectGetWidth(scrollView.bounds) / image.size.width, CGRectGetHeight(scrollView.bounds) / image.size.height);
        CGSize scaledImageSize = CGSizeMake(image.size.width * zoomScale, image.size.height * zoomScale);
        
        self.previewImageView.frame = CGRectMake(0.0, 0.0, scaledImageSize.width, scaledImageSize.height);
        scrollView.contentSize = self.previewImageView.bounds.size;
        
        [scrollView centerContentOffset];
    }
    
    [self updateTraits];
}

- (void)updateTraits
{
    UIScrollView *scrollView = self.goldenRatioView.scrollView;
    UIImageView *imageView = self.previewImageView;
    
    CGPoint convertedCenterPoint = [scrollView convertPoint:imageView.center toView:self.goldenRatioView];
    CGPoint relativeCenterOffset = CGPointMake((convertedCenterPoint.x / CGRectGetWidth(scrollView.bounds)) - 0.5,
                                               (convertedCenterPoint.y / CGRectGetHeight(scrollView.bounds)) - 0.5);
    
    self.traits.zoomScale = scrollView.zoomScale;
    self.traits.relativeCenterOffset = relativeCenterOffset;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(__unused UIScrollView *)scrollView
{
    return self.previewImageView;
}

- (void)scrollViewDidEndDragging:(__unused UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self updateTraits];
    }
}

- (void)scrollViewDidEndDecelerating:(__unused UIScrollView *)scrollView
{
    [self updateTraits];
}

- (void)scrollViewDidEndZooming:(__unused UIScrollView *)scrollView withView:(__unused UIView *)view atScale:(__unused CGFloat)scale
{
    [self updateTraits];
}

@end
