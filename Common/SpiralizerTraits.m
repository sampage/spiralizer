//
//  SpiralizerTraits.m
//  Spiralizer
//
//  Created by Sam Page on 14/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "SpiralizerTraits.h"

static NSString const *kSpiralizerTraitsZoomScaleKey = @"kSpiralizerTraitsZoomScaleKey";
static NSString const *kSpiralizerTraitsRelativeCenterOffsetKey = @"kSpiralizerTraitsRelativeCenterOffsetKey";
static NSString const *kSpiralizerTraitsDrawSpiralKey = @"kSpiralizerTraitsDrawSpiralKey";
static NSString const *kSpiralizerTraitsDrawGridKey = @"kSpiralizerTraitsDrawGridKey";

@implementation SpiralizerTraits

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _zoomScale = 1.0;
        _relativeCenterOffset = CGPointMake(0.0, 0.0);
        
        _drawSpiral = YES;
        _drawGrid = YES;
    }
    return self;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        _zoomScale = [[aDecoder decodeObjectForKey:(NSString *)kSpiralizerTraitsZoomScaleKey] doubleValue];
        _relativeCenterOffset = [[aDecoder decodeObjectForKey:(NSString *)kSpiralizerTraitsRelativeCenterOffsetKey] CGPointValue];
        _drawSpiral = [[aDecoder decodeObjectForKey:(NSString *)kSpiralizerTraitsDrawSpiralKey] boolValue];
        _drawGrid =  [[aDecoder decodeObjectForKey:(NSString *)kSpiralizerTraitsDrawGridKey] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:@(_zoomScale) forKey:(NSString *)kSpiralizerTraitsZoomScaleKey];
    [aCoder encodeObject:[NSValue valueWithCGPoint:_relativeCenterOffset] forKey:(NSString *)kSpiralizerTraitsRelativeCenterOffsetKey];
    [aCoder encodeObject:@(_drawSpiral) forKey:(NSString *)kSpiralizerTraitsDrawSpiralKey];
    [aCoder encodeObject:@(_drawGrid) forKey:(NSString *)kSpiralizerTraitsDrawGridKey];
}

@end
