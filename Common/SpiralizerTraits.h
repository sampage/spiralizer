//
//  SpiralizerTraits.h
//  Spiralizer
//
//  Created by Sam Page on 14/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import UIKit;

@interface SpiralizerTraits : NSObject <NSCoding>

@property (nonatomic, assign) CGFloat zoomScale;
@property (nonatomic, assign) CGPoint relativeCenterOffset;

@property (nonatomic, assign, getter=shouldDrawSpiral) BOOL drawSpiral;
@property (nonatomic, assign, getter=shouldDrawGrid) BOOL drawGrid;

@end
