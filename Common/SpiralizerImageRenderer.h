//
//  SpiralizerImageRenderer.h
//  Spiralizer
//
//  Created by Sam Page on 15/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import UIKit;

@class SpiralizerTraits;

@interface SpiralizerImageRenderer : NSObject

- (UIImage *)renderSourceImage:(UIImage *)sourceImage withTraits:(SpiralizerTraits *)traits;

@end
