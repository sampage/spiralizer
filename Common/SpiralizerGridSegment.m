//
//  SpiralizerGridSegment.m
//  Spiralizer
//
//  Created by Sam Page on 16/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "SpiralizerGridSegment.h"

@interface SpiralizerGridSegment ()

@property (nonatomic, assign, readwrite) CGRect rect;
@property (nonatomic, assign, readwrite) CGRectEdge rectEdge;
@property (nonatomic, assign, readwrite) CGFloat lineWidth;

@end

@implementation SpiralizerGridSegment

- (instancetype)initWithRect:(CGRect)rect rectEdge:(CGRectEdge)rectEdge lineWidth:(CGFloat)lineWidth
{
    self = [super init];
    if (self)
    {
        self.rect = rect;
        self.rectEdge = rectEdge;
        self.lineWidth = lineWidth;
    }
    return self;
}

- (CGPoint)drawingStartPoint
{
    switch (self.rectEdge) {
        case CGRectMinXEdge:
            return CGPointMake(CGRectGetMaxX(self.rect), CGRectGetMaxY(self.rect));
        case CGRectMaxXEdge:
            return CGPointMake(CGRectGetMinX(self.rect), CGRectGetMinY(self.rect));
        case CGRectMinYEdge:
            return CGPointMake(CGRectGetMinX(self.rect), CGRectGetMaxY(self.rect));
        case CGRectMaxYEdge:
            return CGPointMake(CGRectGetMaxX(self.rect), CGRectGetMinY(self.rect));
    }
    NSAssert(NO, @"Unknown CGRectEdge provided to %s", __PRETTY_FUNCTION__);
    return CGPointZero;
}

- (CGPoint)drawingEndPoint
{
    switch (self.rectEdge) {
        case CGRectMinXEdge:
            return CGPointMake(CGRectGetMaxX(self.rect), CGRectGetMinY(self.rect));
        case CGRectMaxXEdge:
            return CGPointMake(CGRectGetMinX(self.rect), CGRectGetMaxY(self.rect));
        case CGRectMinYEdge:
            return CGPointMake(CGRectGetMaxX(self.rect), CGRectGetMaxY(self.rect));
        case CGRectMaxYEdge:
            return CGPointMake(CGRectGetMinX(self.rect), CGRectGetMinY(self.rect));
    }
    NSAssert(NO, @"Unknown CGRectEdge provided to %s", __PRETTY_FUNCTION__);
    return CGPointZero;
}

- (UIBezierPath *)spiralPathSegment
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat radius = CGRectGetWidth(self.rect) - (self.lineWidth / 2.0);
    
    switch (self.rectEdge) {
        case CGRectMinXEdge:
            [path addArcWithCenter:CGPointMake(CGRectGetMaxX(self.rect), CGRectGetMaxY(self.rect)) radius:radius startAngle:M_PI endAngle:-M_PI_2 clockwise:YES];
            break;
        case CGRectMaxXEdge:
            [path addArcWithCenter:CGPointMake(CGRectGetMinX(self.rect), CGRectGetMinY(self.rect)) radius:radius startAngle:0.0 endAngle:M_PI_2 clockwise:YES];
            break;
        case CGRectMinYEdge:
            [path addArcWithCenter:CGPointMake(CGRectGetMinX(self.rect), CGRectGetMaxY(self.rect)) radius:radius startAngle:-M_PI_2 endAngle:0.0 clockwise:YES];
            break;
        case CGRectMaxYEdge:
            [path addArcWithCenter:CGPointMake(CGRectGetMaxX(self.rect), CGRectGetMinY(self.rect)) radius:radius startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
            break;
    }
    
    return path;
}

#pragma mark - Debug

- (id)debugQuickLookObject
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:[self drawingStartPoint]];
    [path addLineToPoint:[self drawingEndPoint]];
    
    [path appendPath:[self spiralPathSegment]];

    return path;
}

@end
