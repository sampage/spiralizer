//
//  AppDelegate.m
//  Spiralizer
//
//  Created by Sam Page on 12/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
            

- (BOOL)application:(__unused UIApplication *)application didFinishLaunchingWithOptions:(__unused NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

@end
