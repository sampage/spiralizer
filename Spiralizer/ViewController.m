//
//  ViewController.m
//  Spiralizer
//
//  Created by Sam Page on 12/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import "ViewController.h"
#import "SpiralizerEditingViewController.h"

@interface ViewController ()

@property (nonatomic, strong) SpiralizerEditingViewController *editingViewController;

@end

@implementation ViewController
            
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.editingViewController = [[SpiralizerEditingViewController alloc] initWithNibName:nil bundle:nil];
    
    [self.editingViewController willMoveToParentViewController:self];
    
    [self addChildViewController:self.editingViewController];
    [self.view addSubview:self.editingViewController.view];
    
    [self.editingViewController didMoveToParentViewController:self];
    
    self.editingViewController.image = [UIImage imageNamed:@"landscape"];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
    return self.editingViewController;
}

@end
