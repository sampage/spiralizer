//
//  AppDelegate.h
//  Spiralizer
//
//  Created by Sam Page on 12/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

