//
//  PhotoEditingViewController.m
//  Spiralizer Extension
//
//  Created by Sam Page on 12/08/2014.
//  Copyright (c) 2014 Sam Page. All rights reserved.
//

@import CoreImage;
@import Photos;
@import PhotosUI;

#import "SpiralizerExtensionViewController.h"
#import "SpiralizerEditingViewController.h"

#import "SpiralizerGoldenRatioOverlayView.h"
#import "SpiralizerTraits.h"

#import "SpiralizerImageRenderer.h"

static NSString * const kAdjustmentDataFormatIdentifier = @"com.sampage.Spiralizer.Spiralizer-Extension";
static NSString * const kAdjustmentDataFormatVersion = @"1.0";

@interface SpiralizerExtensionViewController () <PHContentEditingController>

@property (nonatomic, strong) PHContentEditingInput *contentEditingInput;
@property (nonatomic, strong) SpiralizerEditingViewController *editingViewController;

@end

@implementation SpiralizerExtensionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.editingViewController = [[SpiralizerEditingViewController alloc] initWithNibName:nil bundle:nil];
    
    [self.editingViewController willMoveToParentViewController:self];
    [self addChildViewController:self.editingViewController];
    UIView *editingView = self.editingViewController.view;
    [self.view addSubview:editingView];
    [self.editingViewController didMoveToParentViewController:self];
    
    NSDictionary *viewBindings = NSDictionaryOfVariableBindings(editingView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[editingView]|" options:0 metrics:nil views:viewBindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[editingView]|" options:0 metrics:nil views:viewBindings]];
}

#pragma mark - PHContentEditingController

- (BOOL)canHandleAdjustmentData:(__unused PHAdjustmentData *)adjustmentData
{
    return NO;
}

- (void)startContentEditingWithInput:(PHContentEditingInput *)contentEditingInput placeholderImage:(__unused UIImage *)placeholderImage
{
    NSAssert(contentEditingInput.mediaType == PHAssetMediaTypeImage, @"Unsupported media type (%ld) supplied to %s", (long)contentEditingInput.mediaType, __PRETTY_FUNCTION__);
    
    self.contentEditingInput = contentEditingInput;
    
    UIImage *sourcePreviewImage = contentEditingInput.displaySizeImage;
    [self.editingViewController setImage:sourcePreviewImage];
}

- (void)finishContentEditingWithCompletionHandler:(void (^)(PHContentEditingOutput *))completionHandler
{
    PHContentEditingOutput *contentEditingOutput = [[PHContentEditingOutput alloc] initWithContentEditingInput:self.contentEditingInput];
    
    SpiralizerTraits *traits = self.editingViewController.traits;
    SpiralizerImageRenderer *imageRenderer = [[SpiralizerImageRenderer alloc] init];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:traits];
    PHAdjustmentData *adjustmentData = [[PHAdjustmentData alloc] initWithFormatIdentifier:kAdjustmentDataFormatIdentifier
                                                                            formatVersion:kAdjustmentDataFormatVersion
                                                                                     data:data];
    contentEditingOutput.adjustmentData = adjustmentData;

    NSURL *url = self.contentEditingInput.fullSizeImageURL;
    UIImage *fullSizeImage = [UIImage imageWithContentsOfFile:url.path];
    
    UIImage *renderedImage = [imageRenderer renderSourceImage:fullSizeImage withTraits:traits];
    NSData *renderedJPEGData = UIImageJPEGRepresentation(renderedImage, 1.0);
    
    NSError *error = nil;
    BOOL success = [renderedJPEGData writeToURL:contentEditingOutput.renderedContentURL options:NSDataWritingAtomic error:&error];
    
    NSAssert(success && error == nil, @"An error occured when attempting to render image to disk: %@", error);
    
    completionHandler(contentEditingOutput);
}

- (BOOL)shouldShowCancelConfirmation
{
    return NO;
}

- (void)cancelContentEditing
{
}

@end
